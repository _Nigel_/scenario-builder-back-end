package com.roleplaytools.scenariobuilder.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ItemService {

    @Autowired
    private ItemRepository repository;

    public List<Item> getItems() {
        return (List<Item>) repository.findAll();
    }

    public Item getItems(long id) {
        return repository.findById(id).orElseThrow();
    }

    public Item addItem(Item item) {
        return repository.save(item);
    }

    public void removeItem(long id){ repository.deleteById(id); }
}
