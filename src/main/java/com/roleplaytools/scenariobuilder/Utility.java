package com.roleplaytools.scenariobuilder;

import org.springframework.stereotype.Service;

import java.util.Random;
@Service
public class Utility {

    public int randomRange(int min, int max){
        int result = 0;

        Random random = new Random();

        result = random.nextInt((max - min) + 1) - min;

        return result;
    }

}
