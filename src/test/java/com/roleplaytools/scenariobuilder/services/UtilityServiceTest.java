package com.roleplaytools.scenariobuilder.services;

import com.roleplaytools.scenariobuilder.Utility;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class UtilityServiceTest {

    Utility utility = new Utility();

    @Test
    public void randomRangeTest(){
        for(int i=0; i < 10; i++) {
            int value = utility.randomRange(0, 10);

            assertTrue(value > 0);
            assertTrue(value <= 10);
        }
    }
}
